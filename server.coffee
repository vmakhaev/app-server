http = require 'http'
derby = require 'derby'
#express = require 'express'
#vhost = require 'vhost'
serverApp = require './app'

listenCallback = (err) ->
  console.log '%d listening. Go to: http://localhost:%d/', process.pid, process.env.PORT

apps =
  'enerman.ru': ['enerman.ru']
  'enermancorp.com': ['enermancorp.com', 'enerman.hk', 'szenerman.com']
  'yolumy.com': ['yolumy.com', 'iolumi.com']
  'ledtest': ['testled.co']
  'enerman.catalog': ['catalog.enerman.ru', 'catalog.enermancorp.com', 'catalog.enerman.hk', 'catalog.szenerman.com']

module.exports = (app) ->

  createServer = ->
    expressApp = serverApp app

    server = http.createServer expressApp
    server.listen process.env.PORT, listenCallback

  derby.run createServer

  process.on 'uncaughtException', (err) ->
    console.log 'Uncaught exception: ' + err.stack

return
createServer = ->
  expressApp = express()
  for name, domains of apps
    if name is process.env.APP
      expressApp.use vhost 'localhost', serverApp require name
    else
      expressApp.use vhost "#{name}.yolumy.com", serverApp require name
      for domain in domains
        expressApp.use vhost domain, serverApp require name
        expressApp.use vhost "www.#{domain}", serverApp require name

  server = http.createServer expressApp
  server.listen process.env.PORT, listenCallback

derby.run createServer

process.on 'uncaughtException', (err) ->
  console.log 'Uncaught exception: ' + err.stack
