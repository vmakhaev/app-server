path = require 'path'
coffeeify = require 'coffeeify'
derby = require 'derby'
express = require 'express'
session = require 'express-session'
serveFavicon = require 'serve-favicon'
serveStatic = require 'serve-static'
compression = require 'compression'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
MongoStore = require('connect-mongo')(session)
racerBrowserChannel = require 'racer-browserchannel'
liveDbMongo = require 'livedb-mongo'
derby.use require 'racer-bundle'

module.exports = (app, options) ->
  publicDir = path.dirname(app.filename) + '/public'

  sessionStore = new MongoStore url: process.env.MONGO_URL

  sessionMiddleware = session
    secret: process.env.SESSION_SECRET
    store: sessionStore

  store = derby.createStore
    db: liveDbMongo process.env.MONGO_URL + '?auto_reconnect', safe: true

  store.on 'bundle', (browserify) ->
    browserify.transform global: true, coffeeify

    pack = browserify.pack
    browserify.pack = (opts) ->
      detectTransform = opts.globalTransform.shift()
      opts.globalTransform.push detectTransform
      pack.apply this, arguments

  createUserId = (err, req, res, next) ->
    if err
      return next err
    model = req.getModel()
    userId = req.session.userId
    if not userId
      userId = req.session.userId = model.id()
    model.set '_session.userId', userId
    next()

  expressApp = express()
    .use compression()
    .use serveFavicon publicDir + '/favicon.ico'
    .use serveStatic publicDir
    .use cookieParser process.env.SESSION_SECRET
    .use sessionMiddleware
    .use racerBrowserChannel store
    .use store.modelMiddleware()
    .use createUserId
    .use bodyParser()

  if app.middleware
    app.middleware expressApp, store

  expressApp
    .use app.router()

  if app.setup
    app.setup store, app, expressApp, derby

  expressApp.all '*', (req, res, next) ->
    next '403: ' + req.url

  app.writeScripts store, publicDir, extensions: ['.coffee'], ->

  expressApp
